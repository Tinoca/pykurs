# Pikos Python Kurs 12.10.2021


# Code aus dem Hedgedoc
https://md.ha.si/hSddjptRQKGKXFzatiQGRQ#
Erste Version Aufgabe 1
```
from turtle import *
import random

ecken = int(input("Wieviele Ecken dürfen es denn sein? "))

colormode(255)
pensize(10)
for i in range(ecken):
    pencolor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    fd(20)
    lt(360/int(ecken))

exitonclick()
```
Zweite Version; ohne random, mit Farbennamen
```
from turtle import *

ecken = int(input("Wieviele Ecken dürfen es denn sein? "))

farben = ["red", "green", "blue"]

while len(farben) < ecken:
    farben = farben + farben


pensize(10)
for i in range(ecken):
    pencolor(farben[i])
    fd(20)
    lt(360/int(ecken))

exitonclick()
```


Aufgabe 3.1
```
import random

rand1=random.randint(1,20)
rand2=random.randint(1,20)
inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)
answer = input(inputfrage)
answer = int(answer)
    
if answer == rand1 + rand2:
    print("Richtig")
    
else:
    print("Falsch")
```




Aufgabe 3.2

```
import random

ANZAHL_VERSUCHE = 3

rand1=random.randint(1,20)
rand2=random.randint(1,20)
inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)

    
for i in range(ANZAHL_VERSUCHE):
    answer = input(inputfrage)
    answer = int(answer)
    if answer == rand1 + rand2:
        print("Richtig")
        break
    else:
        print("in zeile 89 ist i:", i)
        if i == ANZAHL_VERSUCHE - 1:
            print("Falsch! Das war dein letzter Versuch!")
            break
        print("Falsch! Versuche es nocheinmal! Verbleibende Versuche:", (ANZAHL_VERSUCHE - (i+1)))
        
```