
# Pikos Python Kurs 06.09.2021

Keine Mitschrift; hier nur Pikos Notizen:


## Hausaufgaben

### 1 
Cheat sheet: Siehe Sylvias Cheat sheet

### 3
```
liste = [3, 8, 9, 3, 8, 6, 340]
int( str( liste[-1] )[1] )
int( str(    340    )[1] )
int(    "340"        [1] )
int(       "4"           )
4
```

```
obstliste = ["ananas", "kiwi", "mango", "papaya", "banane", "guave", "gojibeere"]
for frucht in obstliste:    			<- Schleifenkopf
    print("Ich mag " + frucht)			<- Schleifenkörper
```
Mehr Erklärungen:
https://www.youtube.com/watch?v=94UHCEmprCY
https://www.youtube.com/watch?v=6iF8Xb7Z3wQ




## Programme! 
- Unterschied Idle/Shell, Dateien.
- Unterschied Texteditoren und Textverarbeitungsprogramme
- Texteditoren vs. IDEs
- Texteditoren: vim, emacs, notepad, textedit, Sublime
- IDEs: PyCharm, Visual Studio