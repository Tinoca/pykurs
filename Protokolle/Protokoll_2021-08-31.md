
# Pikos Python Kurs 31.08.2021


Mitschrift

# Hausaufgabe
## 1. Was tun chr() und ord()?

chr() erst Mal ohne Inhalt ausprobiert. -> Fehlermeldung sagt, was erforderlich ist: eine Zahl

Verschiedene Zahlen ausprobiert und festgestellt: bei höheren Zahlen gibt Python Buchstaben aus

ord('ui') ausprobiert: Fehler, weil nur ein Zeichen erlaubt

ord('d') ergibt 100

Man kann ord() und chr() ineinander verschachteln, um sie rekursiv zu verwenden
Beispiel: ord(chr(100)) gibt 100 aus

## 2. ASCII-Tabelle

https://tools.piex.at/ascii-tabelle/

Warum hat sie 127 Einträge?
Weil 128 eine 2er Potenz ist (2^7) und weil in Python ab 0 gezählt wird. (0 entspricht Index=1)

Was sagt uns die ASCII-Tabelle über min und max?
Wenn in min() oder max() ein string steht, vergleicht Python die Positionen in der ASCII Tabelle
Beispiel: 
Eingabe: min('!', ' ') 
Ausgabe: ' ' 
'space' hat eine niedrigere Ordnungsnummer als !

## 3. Turtle

Tipp: Unbekannte Python Dinge nachschlagen in den Python Docs
https://docs.python.org/3/library/turtle.html
dort zum Beispiel nach Turtle Shapes suchen

shape('turtle') ausprobieren

Befehle / Kurzbefehle und was sie tun
```
forward(100) -> fd(100): bewegt vorwärts in Richtung der Turtle
right(90) -> rt(90): dreht den Zeiger um 90° nach rechts 
right(180): Turtle dreht sich um
left(90) -> lt(90): dreht Zeiger um 90° nach links

reset() vs clear(): wo ist der Unterschied?
reset() = alles auf Anfang
clear() = löscht die Linie, Turtle bleibt aber auf position
```

statt reset() geht auch undo()

shortcuts für Auswahl von Python-Eingaben
Tab completion (Tab-Taste): vervollständigt Funktion, wenn man die ersten paar Buchstaben tipp
History (alt + P): ruft vorher benutzte Befehle auf

# Neues Kapitel: Listen

Es gibt verschiedene Listen-Typen, die verschiedene Namen haben (dictionarys, tuples, sets) und verschiedene Klammern nutzen
[], {}, {...}

- Man kann Listen mit + zusammenfügen
- Man kann Listen von Listen machen
	dazu zwei Variablen definieren und in eine Variable packen
	Ausgabe erscheint jetzt mit zwei eckigen Klammern
- man kann einzelne Artikel aus der Liste mit einem Index auswählen
- der erste Index ist immer 0
- deshalb muss man in unserem Beispiel '0' eingeben, um 'Brot' ausgeben zu lassen
- den Index muss man sich an die Stelle zwischen den Elementen in der Liste denken (wird später relevant. 
- Beispiel: wenn man dem computer sagt, "gib mir alles zwischen index 1 und 3" gibt er in unserem Beispiel 'Erdnüsse' und 'Käse' aus
- bei Index -1 wird von hinten gezählt

Übung: Einkaufsliste schreiben und ausprobieren was einkaufsliste[0] und einkaufsliste[-2] macht
-2 : gibt das vorletzte Item wieder 
0: gibt das erste item wieder

Was macht einkaufsliste[-2][3] ?
gibt das zeichen an index-Stelle [3] aus dem item mit Indem [-2] aus



# Pikos Idle

Das hier sind die Kommandos, die Piko eingegeben hat:
```
chr()
chr("uiuiui")
chr(65)
chr(44)
chr(0)
chr(1)
chr(89)
chr(100)
chr(150)
ord()
ord(10)
ord("ui")
ord("u")
ord(chr(100))
chr(83)
chr(99)
chr(127)
chr(65)
ord("ö")
max(" ", "!")
max("%", "D")
ord("8")
from turtle import *
forward(100)
shape("turtle")
Tab completion: Vervollständigung
History: Alt+P
fd(100)
rt(20)
fd(10)
clear()
reset()
[] Listen
["Brot", "Erdnüsse", "Käse", "Müsli"]
einkaufsliste = ["Brot", "Erdnüsse", "Käse", "Müsli"]
einkaufsliste
annas_einkaufsliste = ["Apfel", "Katzenfutter", "Schokolade", "Schokolade", "Gurken"]
zusammen = einkaufsliste + annas_einkaufsliste
zusammen
listenliste = [einkaufsliste, annas_einkaufsliste]
listenliste
einkaufsliste
einkaufsliste[1]
einkaufsliste[2]
einkaufsliste[3]
einkaufsliste[4]
einkaufsliste[-1]
einkaufsliste[-0]
einkaufsliste[-2][3]
"Hallo"[5]
"Hallo"[4]
"Hallo"[-1]
indizierung / Index
```
















