Organisatorisches
 

    Gruppen: wenn Gruppenmitglieder wegfallen

    bei sandala oder Piko melden (per Mail)

    Oder, noch besser, eine Notiz in der Gruppenliste hinterlassen:

    Gruppenliste, bitte tragt euch mit einem Nick ein
    Länge der Sessions: 60-90 Minuten, je nach Inhalt


Hausaufgaben

Aufgabe 1

```
>>> 10/(43.5/60*1.61)
8.567144998929106 miles/h
```

Erklärung: 10km/ (43,5min/60min)= 13.793103448275863km/h / 1.61km (weil 1mile/1.61km = 1km)

 #1 – Taschenrechner
#If you run a 10 kilometer race in 43 minutes 30 seconds, what is your average time per mile? What is your average speed in miles per hour? (Hint: there are 1.61 kilometers in a mile).

Weg / Lösung: 


```
>>> t_seconds = 43 * 60 + 30     # Umrechnung der Zeit in Sekunden
>>> t_seconds_km = t_seconds / 10 # Umrechnung auf einen Kilometer, d.h. die Zeit in Sekunden die für 1km benötigt wird 

>>> t_seconds_mile = t_seconds / 10 * 1.61 # Umrechnung km in miles
>>> print(t_seconds_mile)
420.21000000000004
>>> t_min_mile = t_seconds_mile / 60# Umrechnung der Sekunden in Minuten
>>> print(t_min_mile)
7.003500000000001
>>> 
>>> miles_per_hour = 1 / t_seconds_mile * 3600 # Dreisatz für Umrechnung in Meilen/Stunde
>>> print(miles_per_hour)
8.567144998929106
```

Gegeben:10km/1,61= 6,2112miles in 43.5min
a=10/1,61
a              => 6,2112 miles 
b=43,5
c=b/a
c              => 7,0035 min/mile
und 
d=a/b
d             => 0,1428 miles/min 
e=d*60     => 8,5671miles/hour 


Aufgabe 2

Deanna
```
>>> 1024%111
25
>>> b=111
>>> a=b+b+b+b+b+b+b+b+b+11+11+1+1+1
>>> a
1024
```

Andere Lösung:
a = 1+1
a = a+a - und das dann 9x ausführen
--> man nutzt hier die Tatsache, dass man eine Variable auch definieren kann, indem man sie selbst verwendet um Potenzen auszudrücken

julika
meine Variante wäre so:
a = 1
b = a+1
a = b + b
b = a + a
a = b + b
b = a + a
a = b + b
b = a + a
a = b + b
b = a + a
a = b + b
print(a)
1024


ab= 111+111+111+111+111+111+111+111+111          => 999
b=11+11+1+1+1       => 25
a=ab+b 
a           => 1024


Aufgabe 3

Erkenntnisse: print(a + b +c) macht keine Leerzeichen, print(a, b, c) hingegen schon!
(so konnte man sich das space tippen sparen)

main_word= "mistake"
neg= "not"
space = " "
preposition = "to"
verb="repeat"
conjuction = "but"
start = "it is"
indef_article = "a"
def_article = "the"
verb2 = "make"
print(start,neg, indef_article, main_word, preposition, verb2, indef_article, main_word, conjuction, start, indef_article, main_word, preposition, verb, def_article, main_word)

Definiere die Variablen:
main_word, neg, space, preposition , verb, conjuction, start, indef_article, def_article, verb2 =  "mistake" , "not",  " ",  "to", "repeat", "but",  "it is",  "a", "the",  "make"

Satz ausgeben:
print(start, neg,  indef_article,   main_word, preposition,  verb2,  indef_article ,  main_word,  conjuction, start, indef_article, main_word, preposition, verb, def_article, main_word)

=> it is not a mistake to make a mistake but it is a mistake to repeat the mistake


Alternativ:
Definiere die Variablen wie eben und:
aMis= indef_article + main_word

print(start, neg, aMis, preposition, verb2, aMis, conjuction, start, aMis, preposition, verb, def_article, main_word)

=> it is not a mistake to make a mistake but it is a mistake to repeat the mistake



Theorie

Variablen & Assignment (=zuweisen)
- einer Variable etwas zuweisen
- Variablennamen sind wichtig für die Menschen die nachher euren Code lesen müssen (auch euch selbst!)
- man kann eine Variable in derselben Zeile umdefinieren (siehe Aufgabe 2, a = a +a)
- Spiel: valider Variablenname?

    currentBalance --ist ein Fall von Camelcase, das funktioniert in Python, aber nicht überall

    current balance --funktioniert nicht, wegen dem leerzeichen

    balance4 --geht

    balance --geht

    42 --geht nicht, kann man nicht in eine Schachtel packen

    BALANCE --Großbuchstaben sind ok, aber haben eine besondere Bedeutung (Konstante)

    'hello' --geht nicht, ist etwas was IN eine Variable gehört

    current_balance --geht

    _balance --geht

    current-balance --geht nicht, Subtraktion

    total_$um --$ ist als Sonderzeichen in Variablen nicht erlaubt

    4balance --geht nicht, fängt mit einer Zhal an

..muss man nicht auswendig können, ist nur wichtig für die Fehlersuche

Funktionen
besteht aus einer Funktion (print), Klammern, und etwas zu tun in den Klammern 
z.B. print('hello') 
oder len(12345678) - gibt die länge eines Strings zurück
oder type(7) - gibt den datentyp der Sache in den klammern zurück
Es gibt auch funktionen mit mehr als einem Argument (Argument = das Dings in den Klammern)
z.B. max(7,8,9) - gibt das maximale Argument zurück
oder int(a) - gibt den Wert der Variable a als Integer (=eine Zahl mit der man rechnen kann) zurück
string(a) - gibt den Wert der Variable a als String (=Zeichenfolge) zurück

Lustigerweise kann max auch mit Strings umgehen!
Test:
```
>>> a = 'tschüss'
>>> b = 'moin'
>>> max(a,b)
'tschüss'
```

    Die Sortierung läuft anhand eines Index

    Grossbuchstaben kleineren Index als Sonderzeichen

    und Kleinbuchstaben haben den hoechsten Index

    Indexwerte der Buchstaben werden von links nach rechts miteinander verglichen


Funktionen verketten
Man kann Funktionen ineinander verschachteln. 
Beispiel: der faule Geburtstagsgruß
alter = "74"
print("Nächstes Jahr wirst du", str(int(alter)+1) , "Jahre alt sein")

Leerzeichen / white space
5+2 ist das gleiche wie 5 + 2 
Python ist nett und ignoriert Leerzeichen :)
außer wenn sie Teil eines definierten strings sind "a" ist etwas anderes als " a"

Indents (Einrückungen) - führen auch mal zu Fehlern


Hausaufgaben
Finden sich wieder im github
https://gitlab.com/sudo_piko/pykurs

Pikos IDLE
```
>>> 11%10
1
>>> a = 25
>>> a
25
>>> a = 25 + 25
>>> a
50
>>> b = a
>>> b
50
>>> b = a + a
>>> b
100
>>> a = 1
>>> b = a + a
>>> b
2
>>> a = b + b
>>> a
4
>>> b = a + a
>>> b
8
>>> 
>>> #aufgabe 3
>>> d = "dies"
>>> i = "ist"
>>> g = "gut"
>>> print(d, i, g)
dies ist gut
>>> print(d + i + g)
diesistgut
>>> print(3 + 5 + 6)
14
>>> print(3, 5, 6)
3 5 6
>>> d, i, g = "dies", "ist", "gut"
>>> assignment
KeyboardInterrupt
>>> zahl1 = 42
>>> zahl0 = "lustigeswort"
>>> zahl2 = 14 + 9
>>> zahl1 = zahl1 - 2
>>> zahl1
40
>>> 'currentBalance', 'current balance', 'balance4', 'balance', '42', 'BALANCE', "'hello'", 'current_Balance', '_balance', 'current-balance', 'total_$um', '4balance'
KeyboardInterrupt
>>> current = 4
>>> balance = 3
>>> current-balance
1
>>> current balance = 400
  File "<stdin>", line 1
    current balance = 400
            ^
SyntaxError: invalid syntax
>>> import turtle
>>> import trutle
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'trutle'
>>> print("Hallo")
Hallo
>>> print(zahl0)
lustigeswort
>>> len(zahl0)
12
>>> len("uiuiuiuiuiuiuiuiuiuiuiuiuiuiuiu")
31
>>> len()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: len() takes exactly one argument (0 given)
>>> len("abc")
3
>>> type("abc")
<class 'str'>
>>> type(7)
<class 'int'>
>>> type(zahl0)
<class 'str'>
>>> print(7,8,9)
7 8 9
>>> max(7,8,9)
9
>>> max(8, 12, 19999999, 2)
19999999
>>> type("abc")
<class 'str'>
>>> type(7)
<class 'int'>
>>> type("7")
<class 'str'>
>>> "7"
'7'
>>> int("7")
7
>>> s = "7"
>>> type(s)
<class 'str'>
>>> int(s)
7
>>> type(int(s))
<class 'int'>
>>> s
'7'
>>> s * 2
'77'
>>> int(s) * 2
14
>>> a = 12
>>> str(a)
'12'
>>> str(a) * 2
'1212'
>>> max("Hallo", "hallo")
'hallo'
>>> max("a", "b")
'b'
>>> max("Z", "a")
'a'
>>> max("Z", "a")
'a'
>>> 5 + 2
7
>>> 5+2
7
>>> 5                +2
7
>>> max("Z", " a")
'Z'
>>>  max("Z", " a")
  File "<stdin>", line 1
    max("Z", " a")
IndentationError: unexpected indent
>>> max("Z", "a")
'a'
>>> max("Z", " a")
'Z'
>>>
now
```


