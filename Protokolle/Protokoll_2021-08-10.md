# Pikos Python Kurs EP 02

waldwesens Mitschrift

## Orga
- Wir sollen uns zum gemeinsamen Arbeiten treffen
- Es gibt einen Rocketchat, eher für intermediate-Leute die eigene Ideen diskutieren wollen
- Mailingliste für Orgnaisatorisches (dort bitte keine langen Diskussionen, Ziel: ~2 Mails/Woche): https://lists.haecksen.org/listinfo/python
- Materialien (dürfen wir uns gerne beteiligen): https://gitlab.com/sudo_piko/pykurs
  
Hausaufgaben vom letzten Mal (Hello World + Taschenrechner)
- Hab ich nichts weiter zu rausgefunden
  
## Inhalt
Unterschied shell und IDLE
- Shell ist ein Kommandozeilen-Interpreter, auch Terminal genannt

    Recht schmucklos, man kann (unter Linux) Python daraus starten indem man einfach Python (oder z.B. Python3) eintippt

    dann erscheinen >>> und man kann einfach loslegen (und hat eine Python shell)

    Website zum ausprobieren: https://www.python.org/shell/       https://www.python.org/shell

- Eine IDLE bietet mehr "Luxus"

    Auch IDE - Integrated Development Environment

    zum lernen: Integrated Development and Learning Environment

    Farbliches hervorheben von Syntax

    Erlauben abspeichern von Code

    Trennung von Schreiben + Ausführen von Code

- Was ist Syntax?

    "Spachregeln" beim Programmieren

    Fehler in der Syntax führen oft zu Fehlern

    Es lohnt sich Fehler gründlich zu lesen, auch auf Zeilenangabe achten

Objekte

    String: Zeichenkette (z.B. "Hello World")

      

    Integer: Zahlen

    Man kann Mit Integern rechnen (z.B. 7+8)

- Fehler

    Python ist case-sensitive, Print kann nicht interpretiert werden, print ist ein ordentlicher Befehl

- Variablen

    Man kann einen beliebigen String als Variable definieren, in der man etwas speichern kann

    z.B. Baum = "Hier kommt dein automatischer Baumtext" definieren, dann kommt immer wenn man Baum eingibt und enter drückt "Hier kommt dein automatischer Baumtext"

    Variablen sollten so benannt werden, dass man sie leicht  wiedererkennt / später selbst noch weiß, was man dahinter versteckt hat.

    Am Ende sind Variablen nichts anderes als Kisten, die Variablennamen nichts anderes als ein Name für die Kiste.

- Neue Hausaufgaben

    https://gitlab.com/sudo_piko/pykurs/-/blob/main/Aufgaben_2021-08-10.md 

      

    Beim nächsten Mal sollen Ergebnisse von einer vorgestellt werden 

      

- Zur Erklärung: Gitlab

    Ein Ort wo man Software gut teilen kann

    Soll uns zur Nutzung von so etwas anregen :)

    Gruppenliste, bitte tragt euch mit einem Nick ein

    https://md.darmstadt.ccc.de/uUxnBOlqS8eKN09CfTd3hQ?edit



# PIKOS IDLE
```

Python 3.9.6 (default, Jun 30 2021, 10:22:16) 
[GCC 11.1.0] on linux
Type "help", "copyright", "credits" or "license()" for more information.
>>> print("Hello world")
Hello world
>>> IDLE Integrated Development and Learning Environment
KeyboardInterrupt
>>> print("Hello"))
SyntaxError: unmatched ')'
>>> 5 + * 2
SyntaxError: invalid syntax
>>> type(7)
<class 'int'>
>>> type(0)
<class 'int'>
>>> type("Hello world")
<class 'str'>
>>> 'uearntea'
'uearntea'
>>> 'uaiedtrn"u .eDRTEANFGH:-()(-)iea'
'uaiedtrn"u .eDRTEANFGH:-()(-)iea'
>>> 'He said: "It\'s mine!"'
'He said: "It\'s mine!"'
>>> print("hello") # Das hier ist ein Dings.
hello
>>> 7+8
15
>>> "hallo" + "welt"
'hallowelt'
>>> "hallo" * 3
'hallohallohallo'
>>> "hallo" * "welt"
Traceback (most recent call last):
  File "<pyshell#13>", line 1, in <module>
    "hallo" * "welt"
TypeError: can't multiply sequence by non-int of type 'str'
>>> Print("hallo")
Traceback (most recent call last):
  File "<pyshell#14>", line 1, in <module>
    Print("hallo")
NameError: name 'Print' is not defined
>>> hallo = "Hallo liebe Welt!"
>>> hallo
'Hallo liebe Welt!'
>>> hallo * 3
'Hallo liebe Welt!Hallo liebe Welt!Hallo liebe Welt!'
>>> zahl = "Ich mag Schokolade."
>>> satz = "Ich mag Schokolade."
>>> satz * 3
'Ich mag Schokolade.Ich mag Schokolade.Ich mag Schokolade.'
>>> satz + hallo
'Ich mag Schokolade.Hallo liebe Welt!'
>>> satz + hallo + satz
'Ich mag Schokolade.Hallo liebe Welt!Ich mag Schokolade.'
>>> satz + " " + hallo + " " + satz
'Ich mag Schokolade. Hallo liebe Welt! Ich mag Schokolade.'
>>> leerzeichen = " "
>>> satz + leerzeichen + hallo + leerzeichen + satz
'Ich mag Schokolade. Hallo liebe Welt! Ich mag Schokolade.'
>>> "Ich mag \n Schokolade."
'Ich mag \n Schokolade.'
>>> print("Ich mag \n Schokolade")
Ich mag 
 Schokolade
>>> 1000 + 24
1024
>>> neg = "not"
>>> space = " "
>>> main word = "mistake"
SyntaxError: invalid syntax
```
