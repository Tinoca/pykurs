# Aufgaben am 24.08.2021

# 1 - Funktionen
- Was tun die Funktionen chr() und ord()? Probiert mit denen herum (Fehlermeldungen sind Eure Freunde!). Sucht dann im Internet eine Erklärung.
- Übertragt:

83
99
104
246
110

44
32
100
97
115

115
32
73
104
114

32
100
97
32
115

101
105
100
33

# 2 – ASCII
- Sucht eine ASCII-Tabelle und schaut sie an! Vergleicht sie mit der letzten Aufgabe; was ist mit dem ö? Warum hat sie wohl genau so viele Einträge, wie sie hat?
- Was könnt Ihr jetzt über die Funktionen max() und min() vermuten? Probiert das mit den Sonderzeichen in der ASCII-Tabelle aus. 

# 3 - Turtle
- führt in der Idle aus: (Und lasst Euch von aufploppenden Fenstern nicht beirren; schließt sie aber nicht, sondern schiebt sie zur Seite, so dass Ihr sie noch sehen könnt)
``` 
from turtle import *
forward(50)
right(30)
forward(50)
right(60)
pencolor("red")
forward(50)
pensize(5)
forward(10)
reset()
```
Es ist wichtig, dass Ihr diese Kommandos abtippt und nicht kopiert, sodass Ihr die Syntax einübt.
- Malt
  - ein blaues Haus vom Nikolaus!
  - noch etwas anderes lustiges!

Wenn sich da Fehlermeldungen auftun, dann schaut nochmal ganz genau hin, ob alle Zeichen genau stimmen. 
Wenn es dann immer noch nicht geht, meldet Euch bei Piko mitsamt der Zeile, die die Fehlermeldung auslöst und der Fehlermeldung selbst.