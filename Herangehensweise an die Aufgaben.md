# Herangehensweise an die Aufgaben

Versucht zunächst, die Aufgaben alleine zu lösen. 
Trefft Euch dann in der Gruppe und besprecht die Aufgaben. 
Wenn Ihr sie auch gemeinsam nicht lösen könnt, schlagt im Internet nach. Erklärt die Lösung in der Gruppe bzw. versucht. die Lösung gemeinsam zu verstehen. 

Wenn die Aufgaben trotzdem zu schwer sind oder Ihr noch weiterführende Fragen habt, fragt am nächsten Dienstag nach. 

